package com.gened;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.oracle.javafx.jmx.json.JSONReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gened.nlp.Processor;

import com.gened.Descriptions;
import sun.security.krb5.internal.crypto.Des;

import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

@Controller
public class MainController {

    private Processor processor;
    private  String errorMessage = "";

    public MainController(){
        try {
            this.processor = new Processor();
        }catch (IOException e){
            this.errorMessage += " \n IOException: " + e.getMessage();
            this.processor = null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<String> form() {

        String form =
                "<script\n" +
                        "  src=\"https://code.jquery.com/jquery-3.1.1.min.js\"\n" +
                        "  integrity=\"sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=\"\n" +
                        "  crossorigin=\"anonymous\"></script>\n" +
                "<script>\n"+
                    "function addInput(event){ \n" +
                        "event.stopPropagation();\n" +
                        "event.preventDefault();\n" +
                        "console.log('addInput');\n" +

                        "var form = document.querySelector('form'); \n" +
                        "var length = document.getElementsByTagName('textarea').length; \n" +
                        "var textArea = document.createElement('textarea');\n" +

                        "textArea.setAttribute('name', 'desc-'+length);\n" +
                        "form.appendChild(textArea);\n" +
                    "}" +

                    "function submit(event){\n" +
                        "event.stopPropagation();\n" +
                        "event.preventDefault();\n" +

                        "var data = {descriptions: []};\n" +

                        "var textAreas = document.getElementsByTagName('textarea');\n" +
                        "console.log(textAreas);" +

                        "for(var i = 0; i < textAreas.length; i++){\n" +
                            "data.descriptions.push(textAreas[i].value);\n" +
                        "}" +

                        "console.log(data);\n" +
                        "let headers = new Headers();" +
                        "headers.append('Content-Type', 'application/json');"+

                        "fetch('/', { \n" +
                            "method: 'POST',\n" +
                            "body: JSON.stringify(data),\n" +
                            "headers: headers" +
                        "})" +
                        ".then(function(res){\n" +
                            "return res.text();\n" +
                        "})" +
                        ".then(function(json){\n" +
                            "let el = document.querySelector('#message');\n" +
                            "el.innerHTML = json;\n" +
                        "});\n"+
                    "}"+
                    "(function(){\n" +
                        "$(document).ready(function(){\n" +
                            "document.querySelector('#formSubmit').onclick = submit;\n" +
                            "document.querySelector('#addInput').onclick = addInput;\n" +
                        "});\n" +
                    "})();\n"+
                "</script>\n" +
                "<div id='message'></div>\n"+
                "<div>\n" +
                    "<form action='' onsubmit='return false;'>\n"+
                        "<label for='desc'>Description</label>\n" +
                        "<br>\n"+
                        "<textarea name='desc' style='border-radius:5px; width=300px; height=150px;'></textarea>\n" +
                        "<br>\n"+
                        "<button id='formSubmit'>Submit</button>\n"+
                        "<button id='addInput'>Add Desc</button>\n"+
                    "</form>\n"+
                "</div>";

        return new ResponseEntity<String>(form, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<String> rootPost(@RequestBody Descriptions descriptions) {
        if(this.processor == null) return new ResponseEntity<String>("There was an error: \n" + errorMessage, HttpStatus.OK);

        String retVal = "";

        try {
            retVal = processor.getCategory(descriptions.descriptions);
        }catch(Exception e){
            retVal = e.toString();

            e.printStackTrace();
        }

        return new ResponseEntity<String>(retVal, HttpStatus.OK);
    }

}