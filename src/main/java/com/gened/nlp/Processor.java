package com.gened.nlp;

/**
 * Created by kevindiem on 2/21/17.
 */


import opennlp.tools.doccat.*;
import opennlp.tools.util.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.*;


public class Processor {

    private DocumentCategorizerME categorizer;

    public Processor() throws IOException{

        this.categorizer = new DocumentCategorizerME(train());
    }

    public String getCategory(String[] desc){

        double[] outcomes = categorizer.categorize(desc[0].split(" "));

        HashMap<String, Double> x = (HashMap<String, Double>) categorizer.scoreMap(desc);

        SortedMap<Double, Set<String>> sortedScores =  categorizer.sortedScoreMap(desc[0].split(" "));

        String category  = categorizer.getBestCategory(outcomes);

        String retVal = "{ \"bestCategory\": \"" + category + "\", \"results\": [";

        //Iterator m = x.entrySet().iterator();
        Iterator m = sortedScores.entrySet().iterator();

        while(m.hasNext()){
            //Map.Entry<String, Double> entry = (Map.Entry<String, Double>) m.next();
            Map.Entry<Double, Set<String>> entry = (Map.Entry<Double, Set<String>>) m.next();

            //retVal += entry.getKey() + " " + entry.getValue() + "<br>";
            String temp = "";

            for (String xas:entry.getValue()
                 ) {
                temp += "\"" + xas + "\", ";

            }
            retVal += "{\"categories\": [" + temp.substring(0, temp.length() - 2) + "], \"score\": " + entry.getKey() + "},";
        }

        retVal = retVal.substring(0, retVal.length() - 1) + "]}";

        return retVal;
    }

    public DoccatModel train(){
        Resource resource = new ClassPathResource("newst_model_oo.txt");
        try {
            InputStreamFactory isf = new MarkableFileInputStreamFactory(resource.getFile());

            ObjectStream<String> lineStream = new PlainTextByLineStream(isf, "UTF-8");

            ObjectStream<DocumentSample> sampleStream = new DocumentSampleStream(lineStream);

            TrainingParameters t = new TrainingParameters();

            t.put(t.ITERATIONS_PARAM, "15000");

            DoccatModel m = DocumentCategorizerME.train("en", sampleStream, t, new DoccatFactory());

            return m;

        }catch (IOException e){
            System.out.println(e.toString());
            return null;
        }
    }
}
